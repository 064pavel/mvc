<?php

namespace App\Services;

class View
{
    public static function page(string $page): void
    {
        $filePath = __DIR__ . "/../../views/" . $page . ".php";
        if (!file_exists($filePath)) {
            throw new \RuntimeException("File not found: $filePath");
        }
        require_once $filePath;
    }
}