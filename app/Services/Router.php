<?php

namespace App\Services;

class Router
{
    private static array $list = [];

    public static function get(string $uri, $class): void
    {
        if (class_exists($class)) {
            self::$list[] = [
                "uri" => $uri,
                "class" => $class,
                "post" => false
            ];
        } else {
            throw new \InvalidArgumentException("Class $class does not exist");
        }
    }

    public static function post(string $uri, $class): void
    {
        if (class_exists($class)) {
            self::$list[] = [
                "uri" => $uri,
                "class" => $class,
                "post" => true
            ];
        } else {
            throw new \InvalidArgumentException("Class $class does not exist");
        }
    }

    public static function enable(): void
    {
        $query = $_SERVER['REQUEST_URI'];

        foreach (self::$list as $route) {

            if ($route['uri'] === $query) {

                if (!class_exists($route['class'])) {
                    self::not_found();
                    return;
                }

                try {
                    $action = new $route['class']();
                } catch (\Throwable $e) {
                    self::error($e->getMessage());
                    return;
                }

                if ($route['post'] === true && $_SERVER['REQUEST_METHOD'] === "POST") {
                    $action($_POST);
                } else {
                    $action();
                }

                return;
            }
        }

        self::not_found();
    }

    private static function not_found(): void
    {
        $filePath = __DIR__ . "/../../views/not_found.php";
        if (!file_exists($filePath)) {
            throw new \RuntimeException("File not found: $filePath");
        }
        require_once $filePath;
    }


    private static function error(string $message): void
    {
        http_response_code(500);
        echo "Error: $message";
    }
}