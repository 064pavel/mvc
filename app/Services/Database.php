<?php

namespace App\Services;

use RedBeanPHP\R as DB;

class Database
{
    public static function connect(): void
    {
        $config = require_once __DIR__ . "/../../config/database.php";


        DB::setup('mysql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['db'] . '', $config['username'], $config['password']);

        if (!DB::testConnection()){
            die('DB CONNECTION ERROR');
        }
    }
}