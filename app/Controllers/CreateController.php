<?php

namespace App\Controllers;

use App\Services\View;

class CreateController
{

    public function __invoke(): void
    {
        View::page('create');
    }

}