<?php

namespace App\Controllers;

use App\Services\View;

class IndexController
{
    public function __invoke(): void
    {
        View::page('index');
    }
}