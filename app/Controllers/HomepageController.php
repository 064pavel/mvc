<?php

namespace App\Controllers;

use App\Services\View;

class HomepageController
{
    public function __invoke(): void
    {
        View::page('homepage');
    }
}