<?php

namespace App\Controllers;

use App\Services\View;

class EditController
{
    public function __invoke(): void
    {
        View::page('edit');
    }
}