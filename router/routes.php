<?php

use App\Controllers\CreateController;
use App\Controllers\DeleteController;
use App\Controllers\HomepageController;
use App\Controllers\EditController;
use App\Controllers\UpdateController;
use App\Services\Router;
use App\Controllers\IndexController;
use App\Controllers\StoreController;

// На данный момент поддерживает только однометодные контроллеры

Router::get('/', HomepageController::class);
Router::get('/users', IndexController::class);
Router::get('/users/create', CreateController::class);
Router::get('/users/edit', EditController::class);

Router::post('/users/store', StoreController::class);
Router::post('/users/update', UpdateController::class);
Router::post('/users/delete', DeleteController::class);

Router::enable();
